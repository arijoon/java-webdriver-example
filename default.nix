{ 
  sources ? import ./nix/sources.nix
}:
let
  pkgs = import sources.nixpkgs {
    overlays = [];
    config = {};
  };

  inherit (pkgs) 
    mkShell
    stdenv;

  buildInputs = with pkgs; [
    jdk17
    # 7.6
    gradle
    firefox
  ];
in stdenv.mkDerivation {
  name = "java-playground";
  inherit buildInputs;

  src = null;
  dontUnpack = true;

  passthru = {
    devShell = mkShell {
      name = "java-playround";
      inherit buildInputs;

      shellHook = ''
        # export JAVA_TOOL_OPTIONS='-Dwebdriver.gecko.driver="${pkgs.geckodriver}/bin/geckodriver"'
      '';
    };
  };
}