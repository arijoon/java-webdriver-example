package demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;

import org.junit.jupiter.api.BeforeAll;

@TestInstance(Lifecycle.PER_CLASS)
class AppTest {
  private WebDriver driver;

  @BeforeAll
  public void setupDriver() {
    driver = new FirefoxDriver();
  }

  @Test
  void appWorks() {
    driver.get("https://www.bing.com");

    WebElement elem = driver.findElement(By.name("q"));
    elem.sendKeys("bing sucks");
    elem.submit();

    String expectedTitle = "bing sucks - Search";

    Boolean hasExpectedTitle = new WebDriverWait(driver, Duration.ofSeconds(10))
            .until(ExpectedConditions.titleIs(expectedTitle));

    assertTrue(hasExpectedTitle);
  }
}
